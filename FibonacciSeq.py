print ("Type 'fib(n)' to specify the function")
def fib(n):
    if n < 2:
        return n
    
    return fib(n-1) + fib(n-2)
